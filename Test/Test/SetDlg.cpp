// SetDlg.cpp: ���� ����������
//

#include "stdafx.h"
#include "Test.h"
#include "SetDlg.h"
#include "afxdialogex.h"


// ���������� ���� CSetDlg

IMPLEMENT_DYNAMIC(CSetDlg, CDialogEx)

CSetDlg::CSetDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSetDlg::IDD, pParent)
	, m_RedRadio(0)
{

}

CSetDlg::~CSetDlg()
{
}

void CSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RED_RADIO, m_RedRadio);
	DDV_MinMaxInt(pDX, m_RedRadio, 0, 2);
}


BEGIN_MESSAGE_MAP(CSetDlg, CDialogEx)
	ON_BN_CLICKED(IDC_RED_RADIO, &CSetDlg::OnBnClickedRedRadio)
	ON_BN_CLICKED(IDC_GREEN_RADIO, &CSetDlg::OnBnClickedGreenRadio)
	ON_BN_CLICKED(IDC_BLUE_RADIO, &CSetDlg::OnBnClickedBlueRadio)
END_MESSAGE_MAP()


// ����������� ��������� CSetDlg


void CSetDlg::OnBnClickedRedRadio()
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
}


void CSetDlg::OnBnClickedGreenRadio()
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
}


void CSetDlg::OnBnClickedBlueRadio()
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
}
