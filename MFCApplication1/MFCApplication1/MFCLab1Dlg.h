
// MFCLab1Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CLab1Dlg
class CLab1Dlg : public CDialogEx
{
// ��������
public:
	CLab1Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCAPPLICATION1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnExitBtn();
	afx_msg void OnSayhelloBtn();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedBeep();
};
