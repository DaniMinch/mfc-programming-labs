
// MFCLab6Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMFCLab6Dlg
class CMFCLab6Dlg : public CDialogEx
{
// ��������
public:
	CMFCLab6Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB6_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString m_ResultsEdit;
	afx_msg void OnBnClickedExitButton();
	afx_msg void OnBnClickedOkcancelButton();
	afx_msg void OnBnClickedYesnocancelButton();
	afx_msg void OnBnClickedRetrycancelButton();
	afx_msg void OnBnClickedYesnoButton();
};
