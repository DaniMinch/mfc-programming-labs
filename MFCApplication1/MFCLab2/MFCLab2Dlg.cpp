
// MFCLab2Dlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "MFCLab2.h"
#include "MFCLab2Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CMFCLab2Dlg



CMFCLab2Dlg::CMFCLab2Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCLab2Dlg::IDD, pParent)
	, m_EnabledCheck(TRUE)
	, m_VisibleCheck(TRUE)
	, m_TestEdit(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCLab2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Control(pDX, IDC_TEST_EDIT, m_TestEdit);
	//  DDX_Check(pDX, IDC_VISIBLE_CHECK, m_EnableCheck);
	//  DDX_Text(pDX, IDC_TEST_EDIT, m_Edit);
	DDX_Check(pDX, IDC_ENABLED_CHECK, m_EnabledCheck);
	DDX_Check(pDX, IDC_VISIBLE_CHECK, m_VisibleCheck);
	//  DDX_Text(pDX, IDC_TEST_EDIT, m_TestEdit);
	DDX_Text(pDX, IDC_TEST_EDIT, m_TestEdit);
}

BEGIN_MESSAGE_MAP(CMFCLab2Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_EXIT, &CMFCLab2Dlg::OnBnClickedExit)
	ON_BN_CLICKED(IDC_TEST, &CMFCLab2Dlg::OnTestButton)
	ON_BN_CLICKED(IDC_CLEAR, &CMFCLab2Dlg::OnBnClickedClear)
	ON_BN_CLICKED(IDC_VISIBLE_CHECK, &CMFCLab2Dlg::OnBnClickedVisibleCheck)
	ON_BN_CLICKED(IDC_ENABLED_CHECK, &CMFCLab2Dlg::OnBnClickedEnabledCheck)
	ON_EN_CHANGE(IDC_TEST_EDIT, &CMFCLab2Dlg::OnEnChangeTestEdit)
END_MESSAGE_MAP()


// ����������� ��������� CMFCLab2Dlg

BOOL CMFCLab2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMFCLab2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMFCLab2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMFCLab2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCLab2Dlg::OnBnClickedExit()
{
	// TODO: �������� ���� ��� ����������� �����������
	OnOK();
}


void CMFCLab2Dlg::OnTestButton()
{
	// TODO: �������� ���� ��� ����������� �����������
	m_TestEdit = "This is a test";
	UpdateData(FALSE);
}


void CMFCLab2Dlg::OnBnClickedClear()
{
	// TODO: �������� ���� ��� ����������� �����������
	m_TestEdit = "";
	UpdateData(FALSE);
}


void CMFCLab2Dlg::OnBnClickedVisibleCheck()
{
	// TODO: �������� ���� ��� ����������� �����������
	UpdateData(TRUE);
	if (m_VisibleCheck)
		GetDlgItem(IDC_TEST_EDIT)->ShowWindow(SW_SHOW);
	else
		GetDlgItem(IDC_TEST_EDIT)->ShowWindow(SW_HIDE);
}


void CMFCLab2Dlg::OnBnClickedEnabledCheck()
{
	// TODO: �������� ���� ��� ����������� �����������
	////�������� �������� ���������� ��������� ����������,
	////(���������� ����� ���������� ���������� ��������� ����������)
	UpdateData(TRUE);
	///���� ������� ������ Enable ������� ���� �������������� �������
	///� ���� ��� - �� ���������
	if(m_EnabledCheck==TRUE)
		GetDlgItem(IDC_TEST_EDIT)->EnableWindow(SW_SHOW);
	else
		GetDlgItem(IDC_TEST_EDIT)->EnableWindow(SW_HIDE); 
}


void CMFCLab2Dlg::OnEnChangeTestEdit()
{
	// TODO:  ���� ��� ������� ���������� RICHEDIT, �� ������� ���������� �� �����
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  �������� ��� �������� ����������
	///�������� ����������
	UpdateData(TRUE);
	///������ ���������� ���� CString, ��������� �� ��������
	///���������� m_TestEdit � ��������� ������� �������� � �������
	///�������.
	CString UpperValue;
	UpperValue=m_TestEdit;
	UpperValue.MakeUpper();

	///���� � ���� �������������� ���������� PAINT
	///����������� �������� PAINT � ���� �������������� ���������� ������.
	if(UpperValue=="PAINT")
	{
		system("mspaint.exe");
		m_TestEdit="";
		UpdateData(FALSE);
	}

	///���� � ���� �������������� ���������� CALCULATOR
	///����������� ����������� � ���� �������������� ���������� ������.
	if(UpperValue=="CALCULATOR")
	{
		system("calc.exe");
		m_TestEdit="";
		UpdateData(FALSE);
	} 
	if(UpperValue=="BEEP")
	{
		MessageBeep((WORD)-2);
		m_TestEdit="";
		UpdateData(FALSE);
	}
}
