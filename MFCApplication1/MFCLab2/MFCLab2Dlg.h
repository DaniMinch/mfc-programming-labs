
// MFCLab2Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMFCLab2Dlg
class CMFCLab2Dlg : public CDialogEx
{
// ��������
public:
	CMFCLab2Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB2_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	CString m_TestEdit;
//	BOOL m_EnableCheck;
//	CString m_Edit;
	BOOL m_EnabledCheck;
	BOOL m_VisibleCheck;
//	CString m_TestEdit;
	CString m_TestEdit;
	afx_msg void OnBnClickedExit();
	afx_msg void OnTestButton();
	afx_msg void OnBnClickedClear();
	afx_msg void OnBnClickedVisibleCheck();
	afx_msg void OnBnClickedEnabledCheck();
	afx_msg void OnEnChangeTestEdit();
};
