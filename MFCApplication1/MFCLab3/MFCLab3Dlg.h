
// MFCLab3Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMFCLab3Dlg
class CMFCLab3Dlg : public CDialogEx
{
// ��������
public:
	CMFCLab3Dlg(CWnd* pParent = NULL);	// ����������� ����������� 

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB3_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnStnClickedInstructionStatic();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
private:
	int m_PrevX;
	int m_PrevY;
};
