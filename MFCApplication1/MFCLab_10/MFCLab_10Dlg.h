
// MFCLab_10Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMFCLab_10Dlg
class CMFCLab_10Dlg : public CDialogEx
{
// ��������
public:
	CMFCLab_10Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB_10_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileExit();
	afx_msg void OnFileOpen();
private:
	BITMAP bm;
	HBITMAP hbm;
};
