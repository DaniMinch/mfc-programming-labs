
// MFCLab_10Dlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "MFCLab_10.h"
#include "MFCLab_10Dlg.h"
#include "afxdialogex.h"
#include "Showdib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CMFCLab_10Dlg



CMFCLab_10Dlg::CMFCLab_10Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCLab_10Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCLab_10Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCLab_10Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_FILE_EXIT, &CMFCLab_10Dlg::OnFileExit)
	ON_COMMAND(ID_FILE_OPEN, &CMFCLab_10Dlg::OnFileOpen)
END_MESSAGE_MAP()


// ����������� ��������� CMFCLab_10Dlg

BOOL CMFCLab_10Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMFCLab_10Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMFCLab_10Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//���� ������ bmp ����
		if(hbm) { CClientDC dc(this);

		//�������� ��������� �� DC.

		HDC hdc=::GetDC(m_hWnd);
		HDC hdcBits=::CreateCompatibleDC(hdc);

		//������� ������
		SelectObject(hdcBits,hbm);

		//��������� ���������� ����c�� ������ ������
		CRect wdRect;
		GetClientRect(&wdRect);
		CBrush brush;
		brush.CreateSolidBrush(RGB(0,0,0));
		dc.FillRect(&wdRect,&brush);

		//������� �����������
		BitBlt(hdc, 0, 0, bm.bmWidth,bm.bmHeight,hdcBits,0,0, SRCCOPY);

		//������� DC
		DeleteDC(hdcBits);
		::ReleaseDC(m_hWnd,hdc);
		} 
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMFCLab_10Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCLab_10Dlg::OnFileExit()
{
	// TODO: �������� ���� ��� ����������� ������
	OnOK();
}


void CMFCLab_10Dlg::OnFileOpen()
{
	// TODO: �������� ���� ��� ����������� ������
	//��������� � ���� � �����
	LPSTR FileName;
	char FileTitle[100];
	//FileName[0]='\0';

	//������� ������ ����������� ����
	CFileDialog file(TRUE,NULL,"bmp1",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		"Bitmap picture files (*.bmp) |*.bmp|All Files (*.*)|*.*||");
	if(file.DoModal() == IDOK)
	{
		/*file.m_ofn.lpstrFile
		CString name = file.GetFileName();
		name.GetBuffer
		name = name;
		LPCTSTR tmp = (LPCTSTR)name;
		FileName = (LPCSTR)tmp;*/
		FileName = file.m_ofn.lpstrFile;
		//���� ������ �� ��������, �� ���������
		if (FileName[0]=='\0')return;
		//�������� ��������� ���� �� ��� �����
		SetWindowText(FileName);

		//�������� ��������� �� ������ � ������
		HANDLE hdibCurrent1 = OpenDIB(FileName);
		hbm=0;

		//�������� ��������� �� �����������
		hbm=BitmapFromDib(hdibCurrent1,0);

		//�������� ��������� �����������
		GetObject(hbm,sizeof(BITMAP),(LPSTR)&bm);

		//�������� ���������� ����
		CRect wdRect;
		GetClientRect(&wdRect);
		ClientToScreen(&wdRect);

		//�������� ������� ����
		SetWindowPos(NULL,wdRect.left,wdRect.top, bm.bmWidth,bm.bmHeight+50,NULL);

		//������� ��������
		OnPaint();
	}
}
