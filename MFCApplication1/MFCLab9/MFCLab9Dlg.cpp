
// MFCLab9Dlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "MFCLab9.h"
#include "MFCLab9Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CMFCLab9Dlg



CMFCLab9Dlg::CMFCLab9Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCLab9Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_Club = AfxGetApp()->LoadIcon(IDI_CLUB);
	m_Diamond = AfxGetApp()->LoadIcon(IDI_DIAMOND);
	m_Heart = AfxGetApp()->LoadIcon(IDI_HEART);
	m_Spade = AfxGetApp()->LoadIcon(IDI_SPADE); 
	m_Amt_Remaining = 100.0;
}

void CMFCLab9Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CARD1, m_Card1);
	DDX_Control(pDX, IDC_CARD2, m_Card2);
	DDX_Control(pDX, IDC_CARD3, m_Card3);
	DDX_Control(pDX, IDC_CARD4, m_Card4);
	DDX_Control(pDX, IDC_AMT_LEFT, m_Amt_Left);
}

BEGIN_MESSAGE_MAP(CMFCLab9Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_DEALCARDS, &CMFCLab9Dlg::OnBnClickedDealcards)
	ON_BN_CLICKED(IDCANCEL, &CMFCLab9Dlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// ����������� ��������� CMFCLab9Dlg

BOOL CMFCLab9Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMFCLab9Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMFCLab9Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMFCLab9Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCLab9Dlg::OnBnClickedDealcards()
{
	// TODO: �������� ���� ��� ����������� �����������
	//������ ��� ������� ��� ����� ����
	m_Amt_Remaining-=2.00;

	//����� �����
	DealCards();

	//���������� �������
	CalculateWinnings();

	//�������� ��������� �������� Group Box
	CString s;
	s.Format(_T("Amount Remaining $ %.2f"), m_Amt_Remaining);
	m_Amt_Left.SetWindowText(s);
}


void CMFCLab9Dlg::DealCards(void)
{
	//�������� ��������� �������� ���� ����� �����
	for(int i=0; i<4;i++)m_Cards[i]=0;

	//������ ������ � ��������� ���������� ��������
	m_Card1.SetIcon(PickRandomCard());
	m_Card2.SetIcon(PickRandomCard());
	m_Card3.SetIcon(PickRandomCard());
	m_Card4.SetIcon(PickRandomCard());
}


void CMFCLab9Dlg::CalculateWinnings(void)
{
	int pairs=0;
	for(int i=0; i<4; i++)
	{
		if(m_Cards[i]==2)
		{
			if(pairs>0)
			{
				m_Amt_Remaining+=3.00;
				break;
			}
			else
			{
				pairs++;
			}
		}
		else if(m_Cards[i]==3)
		{
			m_Amt_Remaining+=6.00;
			break;

		}
		else if (m_Cards[i]==4)
		{
			m_Amt_Remaining+=9.00;
			break;
		}
	}
}


HICON& CMFCLab9Dlg::PickRandomCard(void)
{
	//TODO: insert return statement here
	//������� ����� ����� ��������� �������
	int num=(rand()%4);
	/*�������������� ���-�� �������� ���� ����� �����*/
	m_Cards[num]++;

	/*� ����������� �� ��������� ������ ������� ����� ������*/
	switch(num)
	{
	case 0: return m_Club;
	case 1: return m_Diamond;
	case 2: return m_Heart;
	}
	return m_Spade;
}


void CMFCLab9Dlg::OnBnClickedCancel()
{
	// TODO: �������� ���� ��� ����������� �����������
	CString s;
	//�������� � ������ ������ ��������
	s.Format(_T("Good game! I have $ %.2f."),m_Amt_Remaining);
	//������ ���� ���������
	MessageBox(s,L"Thank you for game in Lab9!"); 
	CDialogEx::OnCancel();
}
