//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется MFCLab9.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MFCLAB9_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDI_HEART                       134
#define IDI_CLUB                        135
#define IDI_DIAMOND                     136
#define IDI_SPADE                       137
#define IDB_BITMAP1                     140
#define IDB_BITMAP2                     141
#define IDC_STAT                        1000
#define IDC_AMTLEFT                     1001
#define IDC_AMT_LEFT                    1001
#define IDC_CARD4                       1002
#define IDC_CARD1                       1003
#define IDC_CARD2                       1004
#define IDC_CARD3                       1005
#define IDC_DEALCARDS                   1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
