
// MFCLab9Dlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"


// ���������� ���� CMFCLab9Dlg
class CMFCLab9Dlg : public CDialogEx
{
// ��������
public:
	CMFCLab9Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB9_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	HICON m_Club;
public:
//	HICON m_Diamond;
private:
	HICON m_Spade;
	HICON m_Diamond;
	HICON m_Heart;
public:
	CStatic m_Card1;
	CStatic m_Card2;
	CStatic m_Card3;
	CStatic m_Card4;
	CStatic m_Amt_Left;
private:
	double m_Amt_Remaining;
public:
	afx_msg void OnBnClickedDealcards();
private:
	void DealCards(void);
	void CalculateWinnings(void);
	int m_Cards[5];
public:
	HICON& PickRandomCard(void);
	afx_msg void OnBnClickedCancel();
};
