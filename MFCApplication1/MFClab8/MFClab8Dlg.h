
// MFClab8Dlg.h : ���� ���������
//

#pragma once
#include "atltypes.h"
#include "afxwin.h"


// ���������� ���� CMFClab8Dlg
class CMFClab8Dlg : public CDialogEx
{
// ��������
public:
	CMFClab8Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB8_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedClearbutton();
	afx_msg void OnStnClickedPencolor();	
	int m_PenWidth;
	int m_PenStyle;
	CComboBox m_ShapesCombo;

private:
	CRect m_Canvas;
	CPoint m_LineStart;
	CPoint m_LineEnd;
	COLORREF m_PenColor;
	CRect m_PenColorSwatch;
	CPen m_Pen;
	bool m_IsDrawing;
	void DrawShape(bool stretch = false, CPoint = NULL);
	CBrush m_Brush;
	COLORREF m_BrushColor;
	CRect m_BrushColorSwatch;
	CRect m_BrushPreviewSwatch;
	int m_BrushStyle;
public:
	CListBox m_BrushStyleList;
	afx_msg void OnLbnSelchangeList1();
	void PaintBrushPreview(void);
	afx_msg void OnStnClickedBrushcolor();
private:
	CMetaFileDC* m_pMF;
public:
	afx_msg void OnDestroy();
};
