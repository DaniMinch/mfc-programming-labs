
// MFCLab11View.h : ��������� ������ CMFCLab11View
//

#pragma once

#include "resource.h"


class CMFCLab11View : public CFormView
{
protected: // ������� ������ �� ������������
	CMFCLab11View();
	DECLARE_DYNCREATE(CMFCLab11View)

public:
	enum{ IDD = IDD_MFCLAB11_FORM };

// ��������
public:
	CMFCLab11Doc* GetDocument() const;

// ��������
public:

// ���������������
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual void OnInitialUpdate(); // ���������� � ������ ��� ����� ������������

// ����������
public:
	virtual ~CMFCLab11View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()
public:
	BOOL m_EnableTask1Check;
	BOOL m_EnableTask2Check;
	long m_TaskEdit1;
	long m_TaskEdit2;
	BOOL m_EnableTask3Check;
	long m_TaskEdit3;
};

#ifndef _DEBUG  // ���������� ������ � MFCLab11View.cpp
inline CMFCLab11Doc* CMFCLab11View::GetDocument() const
   { return reinterpret_cast<CMFCLab11Doc*>(m_pDocument); }
#endif

