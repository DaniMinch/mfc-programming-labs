
// MFCLab11View.cpp : ���������� ������ CMFCLab11View
//

#include "stdafx.h"
// SHARED_HANDLERS ����� ���������� � ������������ �������� ��������� ���������� ������� ATL, �������
// � ������; ��������� ��������� ������������ ��� ��������� � ������ �������.
#ifndef SHARED_HANDLERS
#include "MFCLab11.h"
#endif

#include "MFCLab11Doc.h"
#include "MFCLab11View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCLab11View

IMPLEMENT_DYNCREATE(CMFCLab11View, CFormView)

	BEGIN_MESSAGE_MAP(CMFCLab11View, CFormView)
	END_MESSAGE_MAP()

	// ��������/����������� CMFCLab11View

	CMFCLab11View::CMFCLab11View()
		: CFormView(CMFCLab11View::IDD)
		, m_EnableTask1Check(FALSE)
		, m_EnableTask2Check(FALSE)
		, m_TaskEdit1(0)
		, m_TaskEdit2(0)
		, m_EnableTask3Check(FALSE)
		, m_TaskEdit3(0)
	{
		// TODO: �������� ��� ��������

	}

	CMFCLab11View::~CMFCLab11View()
	{
	}

	void CMFCLab11View::DoDataExchange(CDataExchange* pDX)
	{
		CFormView::DoDataExchange(pDX);
		DDX_Check(pDX, IDC_ENABLE_TASK1_CHECK, m_EnableTask1Check);
		DDX_Check(pDX, IDC_ENABLE_TASK2_CHECK, m_EnableTask2Check);
		DDX_Text(pDX, IDC_TASK1_EDIT, m_TaskEdit1);
		DDX_Text(pDX, IDC_TASK2_EDIT, m_TaskEdit2);
		DDX_Check(pDX, IDC_ENABLE_TASK3_CHECK, m_EnableTask3Check);
		DDX_Text(pDX, IDC_TASK3_EDIT, m_TaskEdit3);
	}

	BOOL CMFCLab11View::PreCreateWindow(CREATESTRUCT& cs)
	{
		// TODO: �������� ����� Window ��� ����� ����������� ���������
		//  CREATESTRUCT cs

		return CFormView::PreCreateWindow(cs);
	}

	void CMFCLab11View::OnInitialUpdate()
	{
		CFormView::OnInitialUpdate();
		GetParentFrame()->RecalcLayout();
		ResizeParentToFit();
		m_EnableTask1Check=TRUE;
		m_EnableTask2Check=TRUE;
		m_EnableTask3Check=TRUE;
		UpdateData(FALSE);
	}


	// ����������� CMFCLab11View

#ifdef _DEBUG
	void CMFCLab11View::AssertValid() const
	{
		CFormView::AssertValid();
	}

	void CMFCLab11View::Dump(CDumpContext& dc) const
	{
		CFormView::Dump(dc);
	}

	CMFCLab11Doc* CMFCLab11View::GetDocument() const // �������� ������������ ������
	{
		ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCLab11Doc)));
		return (CMFCLab11Doc*)m_pDocument;
	}
#endif //_DEBUG


	// ����������� ��������� CMFCLab11View
