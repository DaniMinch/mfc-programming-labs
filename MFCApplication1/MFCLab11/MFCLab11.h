
// MFCLab11.h : ������� ���� ��������� ��� ���������� MFCLab11
//
#pragma once

#ifndef __AFXWIN_H__
	#error "�������� stdafx.h �� ��������� ����� ����� � PCH"
#endif

#include "resource.h"       // �������� �������


// CMFCLab11App:
// � ���������� ������� ������ ��. MFCLab11.cpp
//

class CMFCLab11App : public CWinApp
{
public:
	CMFCLab11App();


// ���������������
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// ����������
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnIdle(LONG lCount);
};

extern CMFCLab11App theApp;
