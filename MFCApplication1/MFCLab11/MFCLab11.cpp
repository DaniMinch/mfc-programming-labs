
// MFCLab11.cpp : ���������� ��������� ������� ��� ����������.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "MFCLab11.h"
#include "MainFrm.h"

#include "MFCLab11Doc.h"
#include "MFCLab11View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCLab11App

BEGIN_MESSAGE_MAP(CMFCLab11App, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CMFCLab11App::OnAppAbout)
	// ����������� ������� �� ������ � ������� ����������
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// �������� CMFCLab11App

CMFCLab11App::CMFCLab11App()
{
	// ��������� ���������� ������������
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// ���� ���������� ��������� � ���������� ����� Common Language Runtime (/clr):
	//     1) ���� �������������� �������� ��������� ��� ���������� ��������� ������ ���������� ������������.
	//     2) � ����� ������� ��� ���������� ���������� �������� ������ �� System.Windows.Forms.
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// TODO: �������� ���� ������ �������������� ���������� ������� ����������� ��������������; �������������
	// ������ ��� ������: �����������.�����������.����������.���������������
	SetAppID(_T("MFCLab11.AppID.NoVersion"));

	// TODO: �������� ��� ��������,
	// ��������� ���� ������ ��� ������������� � InitInstance
}

// ������������ ������ CMFCLab11App

CMFCLab11App theApp;


// ������������� CMFCLab11App

BOOL CMFCLab11App::InitInstance()
{
	// InitCommonControlsEx() ��������� ��� Windows XP, ���� ��������
	// ���������� ���������� ComCtl32.dll ������ 6 ��� ����� ������� ������ ��� ���������
	// ������ �����������. � ��������� ������ ����� ��������� ���� ��� �������� ������ ����.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// �������� ���� �������� ��� ��������� ���� ����� ������� ����������, ������� ���������� ������������
	// � ����� ����������.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	// ������������� ��������� OLE
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	EnableTaskbarInteraction(FALSE);

	// ��� ������������� �������� ���������� RichEdit ��������� ����� AfxInitRichEdit2()	
	// AfxInitRichEdit2();

	// ����������� �������������
	// ���� ��� ����������� �� ������������ � ���������� ��������� ������
	// ��������� ������������ �����, ���������� ������� �� ����������
	// ���������� ��������� �������������, ������� �� ���������
	// �������� ������ �������, � ������� �������� ���������
	// TODO: ������� �������� ��� ������ �� ���-������ ����������,
	// �������� �� �������� �����������
	SetRegistryKey(_T("��������� ����������, ��������� � ������� ������� ����������"));
	LoadStdProfileSettings(4);  // ��������� ����������� ��������� INI-����� (������� MRU)


	// ��������������� ������� ���������� ����������. ������� ����������
	//  ��������� � ���� ���������� ����� �����������, ������ ����� � ���������������
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CMFCLab11Doc),
		RUNTIME_CLASS(CMainFrame),       // �������� ���� ����� SDI
		RUNTIME_CLASS(CMFCLab11View));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// ��������� ������������� ����������� �������� � ������� �������� ����
	CMFCToolBar::m_bExtCharTranslation = TRUE;

	// �������������� ������ ��������� ������ �� ����������� ������� ��������, DDE, �������� ������
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);



	// ������� ���������������, ��������� � ��������� ������. �������� FALSE ����� ����������, ����
	// ���������� ���� �������� � ���������� /RegServer, /Register, /Unregserver ��� /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// ���� � ������ ���� ���� ���� ����������������, ������� ���������� � �������� ���
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	return TRUE;
}

int CMFCLab11App::ExitInstance()
{
	//TODO: ����������� �������������� �������, ������� ����� ���� ���������
	AfxOleTerm(FALSE);

	return CWinApp::ExitInstance();
}

// ����������� ��������� CMFCLab11App


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// ������� ���������� ��� ������� �������
void CMFCLab11App::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// ����������� ��������� CMFCLab11App





BOOL CMFCLab11App::OnIdle(LONG lCount)
{
	// TODO: �������� ������������������ ��� ��� ����� �������� ������
	/*������� ������� �������� ������ �WinApp::OnIdle(), ����� ��������� ��� ��������� ������*/
	CWinApp::OnIdle(lCount);
	//�������� ��������� �� ������ ���������
	POSITION pos = GetFirstDocTemplatePosition();
	CDocTemplate* pDocTemplate=GetNextDocTemplate(pos);


	//�������� ����������� ��������
	pos=pDocTemplate->GetFirstDocPosition();
	CDocument* pDoc=pDocTemplate->GetNextDoc(pos);


	//�������� ��������� �� �������������
	pos=pDoc->GetFirstViewPosition();
	CMFCLab11View* pView=(CMFCLab11View*) pDoc->GetNextView(pos);


	//���������� ��� �������� �������
	static DWORD PrevTimeTask1=0;
	static DWORD PrevTimeTask2=0;
	static DWORD PrevTimeTask3=0;


	//�������� ������� �����
	DWORD CurrentTime=GetTickCount();


	//�������� ������ �� ��������� ���������
	pView->UpdateData(TRUE);

	//���� ����� ��������� 50 ��, �� ��������� 1
	if(CurrentTime>PrevTimeTask1+50 &&
		pView->m_EnableTask1Check)
	{
		pView->m_TaskEdit1=pView->m_TaskEdit1+1;
		pView->UpdateData(FALSE);
		PrevTimeTask1=CurrentTime;
	}

	//���� ����� ��������� 500 �� � c������ �������, �� ��������� 1
	if(CurrentTime>PrevTimeTask2+500 &&
		pView->m_EnableTask2Check)
	{
		pView->m_TaskEdit2=pView->m_TaskEdit2+1;
		pView->UpdateData(FALSE);
		PrevTimeTask2=CurrentTime;
	}
	//���� ����� ��������� 1000 �� � c������ �������, �� ��������� 1
	if(CurrentTime>PrevTimeTask3+1000 &&
		pView->m_EnableTask3Check)
	{
		pView->m_TaskEdit3=pView->m_TaskEdit3+1;
		pView->UpdateData(FALSE);
		PrevTimeTask3=CurrentTime;
	}
	return TRUE; 
	//return CWinApp::OnIdle(lCount);
}
