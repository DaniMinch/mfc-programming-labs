
// MFCLab5Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMFCLab5Dlg
class CMFCLab5Dlg : public CDialogEx
{
// ��������
public:
	CMFCLab5Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MFCLAB5_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	int m_SpeedEdit;
	afx_msg void OnChangeSpeedEdit();
	afx_msg void OnFileCurrentspeed();
	afx_msg void OnFileExit();
	afx_msg void OnHelpAbout();
	afx_msg void OnHelpSayhello();
};
