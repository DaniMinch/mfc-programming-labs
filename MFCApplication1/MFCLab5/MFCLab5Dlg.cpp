
// MFCLab5Dlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "MFCLab5.h"
#include "MFCLab5Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CMFCLab5Dlg



CMFCLab5Dlg::CMFCLab5Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCLab5Dlg::IDD, pParent)
	, m_SpeedEdit(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCLab5Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SPEED_EDIT, m_SpeedEdit);
	DDV_MinMaxInt(pDX, m_SpeedEdit, 0, 100);
}

BEGIN_MESSAGE_MAP(CMFCLab5Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_SPEED_EDIT, &CMFCLab5Dlg::OnChangeSpeedEdit)
	ON_COMMAND(ID_FILE_CURRENTSPEED, &CMFCLab5Dlg::OnFileCurrentspeed)
	ON_COMMAND(ID_FILE_EXIT, &CMFCLab5Dlg::OnFileExit)
	ON_COMMAND(ID_HELP_ABOUT, &CMFCLab5Dlg::OnHelpAbout)
	ON_COMMAND(ID_HELP_SAYHELLO, &CMFCLab5Dlg::OnHelpSayhello)
END_MESSAGE_MAP()


// ����������� ��������� CMFCLab5Dlg

BOOL CMFCLab5Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CMFCLab5Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMFCLab5Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMFCLab5Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCLab5Dlg::OnChangeSpeedEdit()
{
	// TODO:  ���� ��� ������� ���������� RICHEDIT, �� ������� ���������� �� �����
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  �������� ��� �������� ����������
	UpdateData(TRUE); 
}


void CMFCLab5Dlg::OnFileCurrentspeed()
{
	// TODO: �������� ���� ��� ����������� ������
	char strSpeed[100];
	_itoa_s(m_SpeedEdit,strSpeed,10);
	MessageBox(CA2W(strSpeed)); 
}


void CMFCLab5Dlg::OnFileExit()
{
	// TODO: �������� ���� ��� ����������� ������
	OnOK();
}


void CMFCLab5Dlg::OnHelpAbout()
{
	// TODO: �������� ���� ��� ����������� ������
	CAboutDlg dlg;

	//������� ���������� ������ About
	dlg.DoModal(); 
}


void CMFCLab5Dlg::OnHelpSayhello()
{
	// TODO: �������� ���� ��� ����������� ������
	MessageBox(L"Hello!");
}
